# ChangeLog

mangl changelog

## 1.1.0 2022-05-06
* change windowing library from FreeGLUT to GLFW (suport for Wayland)
* add smartcase search - if only lowercase characters are used, search case-insensitive

## 1.0.0
* initial implementation

